import '../home/home.css'
import '../home/index'
import { Outlet, Link } from 'react-router-dom'


function Home() {

  return (
    <>
      <header>
        <h1>Catálogo de Aves</h1>
        <nav>
          <Link to='/listapassaro'>Buscar Pássaros</Link> | {' '}
          <Link to='/avistamento'>Cadastro de Avistamento</Link> | {' '}
          <Link to='/avistamentos'>Visualizar Avistamentos</Link>
        </nav>
      </header>
      <main>
        <div className='container'>
          <Outlet />
        </div>
      </main>
      <footer>
        <p>Desenvolvido por Rhanna e Micheli</p>
      </footer>
    </>
  );
}

export default Home

