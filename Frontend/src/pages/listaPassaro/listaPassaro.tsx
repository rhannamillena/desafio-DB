import { useEffect, useState } from 'react'
import { List } from '../../components/list/List'
import { Link } from "react-router-dom";
import './index'
import { PassaroDto } from '../../services/passaro/passaro.dto';
import * as PassaroService from "../../services/passaro/passaro.service";
import { EnumRotas } from '../../interfaces/enumRotas';

const ListaPassaro = () => {
  const [listaPassaros, setListaPassaros] = useState<PassaroDto[]>([]);
  const [escolhaFiltro, setEscolhaFiltro] = useState<string>(EnumRotas.Nome);
  const [valorPesquisa, setValorPesquisa] = useState<string>("buscar");

  useEffect(() => {
    listarPassaros();
  }, []);

  async function listarPassaros() {
    const passaros = await PassaroService.get();
    setListaPassaros(passaros);
  }

  async function listarPorParametro() {
    const passaros = await PassaroService.getParametro(valorPesquisa, escolhaFiltro);
    setListaPassaros(passaros)
  }

  return (
    <>
      <header>
        <h1>Lista de passaros</h1>
        <nav>
          <Link to='/home'>Início</Link> | {' '}
          <Link to='/avistamento'>Cadastro de Avistamento</Link> | {' '}
          <Link to='/avistamentos'>Visualizar Avistamentos</Link>
        </nav>
      </header>
      <main>
        <div className='center'>
          <label>
            <p>Filtrar:</p>
            <select
              name="passaro"
              value={escolhaFiltro}
              onChange={(e) => {
                setEscolhaFiltro(
                  e.target.value
                )
              }}
              required
            >
              <option value="DEFAULT">Selecione um filtro</option>
              {Object.entries(EnumRotas).map((a) => <option key={a.toString()} value={a[1]} >{a[0]}</option>)}
            </select>
          </label>
          <label>
            <p>Buscar:</p>
            <input
              value={valorPesquisa}
              onChange={(e) => {
                setValorPesquisa(
                  e.target.value
                )
              }}
            />
          </label>
          <button onClick={() => { listarPorParametro() }}> Localizar </button>

          <List items={listaPassaros} />
        </div>
      </main>
      <footer>
        <p>Desenvolvido por Rhanna e Micheli</p>
      </footer>
    </>
  )
}
export default ListaPassaro;
