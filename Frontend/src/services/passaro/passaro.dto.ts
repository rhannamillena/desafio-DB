export interface PassaroDto {
    _id: string,
    nome: string,
    nomeIngles: string,
    nomeCientifico: string,
    genero: string,
    familia: string,
    tamanho: string,
    habitat: string,
    cor: string,
    __v: string
}