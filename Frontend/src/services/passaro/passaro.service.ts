import { PassaroDto } from "./passaro.dto";
import axios from "axios";
import { EnumRotas } from "../../interfaces/enumRotas";

const uriBase = "http://localhost:8081/passaro";

export async function get(): Promise<PassaroDto[]> {
    const resposta = await axios.get(`${uriBase}/buscar`)

    if (resposta.status !== 200) {
        throw new Error(`Requisição falhou: ${resposta.statusText}`);
    }

    return resposta.data;
}

export async function getParametro(valor:String, rotaFiltroEnum:String): Promise<PassaroDto[]> {
    const resposta = await axios.get(`${uriBase}/${rotaFiltroEnum}/${valor}`)

    if (resposta.status !== 200) {
        throw new Error(`Requisição falhou: ${resposta.statusText}`);
    }

    return resposta.data;
    
}