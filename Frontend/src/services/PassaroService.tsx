import { API } from "../API"
import {passaroInterface} from "../interfaces/passaroInterface"

const getAll = () => API.get<passaroInterface[]>('/passaro/buscar')
const getNome = () => API.get<passaroInterface[]>('/passaro/buscarpornome/{nome}')

export const PassaroService = {
    getAll,
    getNome
}