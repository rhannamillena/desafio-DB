import { AvistamentoDto } from "./avistamento.dto";
import axios from "axios";

const uriBase = "http://localhost:8081/avistamento";

export async function get(): Promise<AvistamentoDto[]> {
    const resposta = await axios.get(`${uriBase}/buscar`)

    if (resposta.status !== 200) {
        throw new Error(`Requisição falhou: ${resposta.statusText}`);
    }

    return resposta.data;
}

export async function post(avistamento: AvistamentoDto): Promise<AvistamentoDto> {
    let formData = {
        local: avistamento.local,
        data: `${avistamento.data}T${avistamento.hora}:00-03:00`,
        hora: `${avistamento.data}T${avistamento.hora}:00-03:00`,
        pagina: avistamento.pagina,
        linha: avistamento.linha,
        coluna: avistamento.coluna,
        passaro: avistamento.passaro._id
    }

    const resposta = await axios.post(uriBase, formData)
        .then(function (resposta) {
            return resposta.data;
        })
        .catch(function (error) {
            throw new Error(`Requisição falhou: ${error}`);
        });

    return resposta;
}