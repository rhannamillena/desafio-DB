import { PassaroDto } from "../passaro/passaro.dto";

export interface AvistamentoDto {
    _id: string,
    local: string,
    data: string,
    hora: string,
    pagina: number,
    linha: number,
    coluna: number,
    passaro: PassaroDto,
    __v: string
}