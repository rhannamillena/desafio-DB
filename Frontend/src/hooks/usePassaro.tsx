import { useCallback, useState } from "react"
import { passaroInterface } from "../interfaces";
import { PassaroService } from "../services/PassaroService"

export const usePassaro = () => {
    const [tasks, setTasks] = useState<passaroInterface[]>([]);
    const getAll = useCallback(async () => {
        const { status, data } = await PassaroService.getAll();
        if (status !== 200) throw new Error();

        setTasks(data);
    }, [])
    return {
        tasks,
        getAll
    }
}