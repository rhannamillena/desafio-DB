import React from 'react';
import ReactDOM from 'react-dom/client';
import './index.css';
import reportWebVitals from './reportWebVitals';
import { BrowserRouter, Route, Routes } from 'react-router-dom'
import Home from '../src/pages/home/home';
import ListaPassaro from './pages/listaPassaro/listaPassaro';
import { AvistamentoForm } from './components/AvistamentoForm';
import App from './App';
import { AvistamentoList } from './components/AvistamentoList';

const root = ReactDOM.createRoot(
  document.getElementById('root') as HTMLElement
);

root.render(
  <React.StrictMode>
    <BrowserRouter>
      <Routes>
        <Route path='/' element={<App />}>
          <Route index element={<Home />} />
          <Route path='/avistamento' element={<AvistamentoForm />} />
          <Route path='/home' element={<Home />} />
          <Route path='/listaPassaro' element={<ListaPassaro />} />
          <Route path='avistamentos' element={<AvistamentoList />} />
        </Route>
      </Routes>
    </BrowserRouter>
  </React.StrictMode>
);
reportWebVitals();
