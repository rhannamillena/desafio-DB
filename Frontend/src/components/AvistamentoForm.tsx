import { useEffect, useState } from "react";
import { Link } from "react-router-dom";
import { AvistamentoDto } from "../services/avistamento/avistamento.dto";
import * as AvistamentoService from "../services/avistamento/avistamento.service";
import { PassaroDto } from "../services/passaro/passaro.dto";
import * as PassaroService from "../services/passaro/passaro.service";

export function AvistamentoForm() {
    const [status, setStatus] = useState("");
    const [message, setMessage] = useState("");
    const [listaPassaros, setListaPassaros] = useState<PassaroDto[]>();
    const [formValues, setFormValues] = useState<AvistamentoDto>({
        _id: "",
        local: "",
        data: "",
        hora: "",
        pagina: 0,
        linha: 0,
        coluna: 0,
        passaro: {
            _id: "",
            nome: "",
            nomeIngles: "",
            nomeCientifico: "",
            genero: "",
            familia: "",
            tamanho: "",
            habitat: "",
            cor: "",
            __v: ""
        },
        __v: ""
    });

    const salvarAvistamento = async () => {
        try {
            await AvistamentoService.post(formValues);
            setMessage("Avistamento criado com sucesso!");
            setStatus("Success");
        } catch (error) {
            setMessage("Ocorreu um erro ao salvar o avistamento!");
            setStatus("Error");
        }
    }

    useEffect(() => {
        async function listarPassaros() {
            const passaros = await PassaroService.get();
            setListaPassaros(passaros);
        }

        listarPassaros();
    }, []);

    return (
        <>
            <header>
                <h1>Novo Avistamento</h1>
                <nav>
                    <Link to='/home'>Início</Link> | {' '}
                    <Link to='/listaPassaro'>Buscar Pássaros</Link> | {' '}
                    <Link to='/avistamentos'>Visualizar Avistamentos</Link>
                </nav>
            </header>

            <main>
                <div className="center">
                    {status === "Error" &&
                        <h3 className="error"> {message} </h3>
                    }

                    {status === "Success" &&
                        <h3 className="success"> {message} </h3>
                    }

                    <form
                        onSubmit={(e) => {
                            e.preventDefault();
                            salvarAvistamento();
                        }}
                    >
                        <fieldset>
                            <label>
                                <p>Local:</p>
                                <input
                                    type="text"
                                    name="local"
                                    value={formValues.local}
                                    onChange={(e) => {
                                        setFormValues({
                                            ...formValues,
                                            local: e.target.value
                                        })
                                    }}
                                    required
                                />
                            </label>
                            <label>
                                <p>Data:</p>
                                <input
                                    type="date"
                                    name="data"
                                    value={formValues.data}
                                    onChange={(e) => {
                                        setFormValues({
                                            ...formValues,
                                            data: e.target.value
                                        })
                                    }}
                                    required
                                />
                            </label>
                            <label>
                                <p>Hora:</p>
                                <input
                                    type="time"
                                    name="hora"
                                    value={formValues.hora}
                                    onChange={(e) => {
                                        setFormValues({
                                            ...formValues,
                                            hora: e.target.value
                                        })
                                    }}
                                    required
                                />
                            </label>
                            <label>
                                <p>Página:</p>
                                <input
                                    type="number"
                                    name="pagina"
                                    value={formValues.pagina}
                                    onChange={(e) => {
                                        setFormValues({
                                            ...formValues,
                                            pagina: parseInt(e.target.value)
                                        })
                                    }}
                                />
                            </label>
                            <label>
                                <p>Linha:</p>
                                <input
                                    type="number"
                                    name="linha"
                                    value={formValues.linha}
                                    onChange={(e) => {
                                        setFormValues({
                                            ...formValues,
                                            linha: parseInt(e.target.value)
                                        })
                                    }}
                                />
                            </label>
                            <label>
                                <p>Coluna:</p>
                                <input
                                    type="number"
                                    name="coluna"
                                    value={formValues.coluna}
                                    onChange={(e) => {
                                        setFormValues({
                                            ...formValues,
                                            pagina: parseInt(e.target.value)
                                        })
                                    }}
                                />
                            </label>
                            <label>
                                <p>Passaro:</p>
                                <select
                                    name="passaro"
                                    value={formValues.passaro._id}
                                    onChange={(e) => {
                                        setFormValues({
                                            ...formValues,
                                            passaro: {
                                                ...formValues.passaro,
                                                _id: e.target.value
                                            }
                                        })
                                    }}
                                    required
                                >
                                    <option value="DEFAULT">Selecione um pássaro</option>
                                    {listaPassaros?.map(passaro => <option key={passaro._id} value={passaro._id}>{passaro.nome}</option>)}
                                </select>
                            </label>
                        </fieldset>
                        <button type="submit">Salvar</button>
                    </form>
                </div>
            </main>
            <footer>
                <p>Desenvolvido por Rhanna e Micheli</p>
            </footer>
        </>
    );
}