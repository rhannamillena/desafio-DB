import { ListItem, ListItemProps } from './ListPassaro';
import {passaroInterface} from "../../interfaces/passaroInterface"
import { PassaroDto } from '../../services/passaro/passaro.dto';

export type ListProps = {
    items: PassaroDto[]

};

export const List: React.FC<ListProps> = ({ items }) => {
  console.log(items)
        return (
          <>
            {items.map((item, index) => (
              <ListItem key={index} {...item}  />
            ))}
         </>
        );
      };