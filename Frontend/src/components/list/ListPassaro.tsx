import { PassaroDto } from '../../services/passaro/passaro.dto';

export type ListItemProps = {
  _id: string,
  nome: string,
  nomeIngles: string,
  nomeCientifico: string,
  genero: string,
  familia: string,
  tamanho: string,
  habitat: string,
  cor: string,
};

export const ListItem: React.FC<PassaroDto> = ({ _id, nome, nomeIngles, nomeCientifico, familia, tamanho, habitat }) => {
  return (
    <div className="col-6">
      <div>
        <body>
          <br />
          <table>
            <tr>
              <td>Nome: {nome} </td>
            </tr>
            <tr>
              <td>Nome em Inglês: {nomeIngles}</td>
            </tr>
            <tr>
              <td>Nome científico: {nomeCientifico}</td>
            </tr>
            <tr>
              <td>Família: {familia}</td>
            </tr>
            <tr>
              <td>Habitat: {habitat}</td>
            </tr>
          </table>
        </body>
      </div>
    </div>
  );
};