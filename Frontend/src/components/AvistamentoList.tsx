import { useEffect, useState } from "react";
import { Link } from "react-router-dom";
import { AvistamentoDto } from "../services/avistamento/avistamento.dto";
import * as AvistamentoService from "../services/avistamento/avistamento.service";

export function AvistamentoList() {
    const [listaAvistamentos, setListaAvistamentos] = useState<AvistamentoDto[]>();

    useEffect(() => {
        async function listarAvistamentos() {
            const avistamentos = await AvistamentoService.get();
            setListaAvistamentos(avistamentos);
        }

        listarAvistamentos();
    }, []);

    const padTo2Digits = (num: number) => {
        return num.toString().padStart(2, '0');
    }

    const formatDate = (date: Date) => {
        return [
            padTo2Digits(date.getDate()),
            padTo2Digits(date.getMonth() + 1),
            date.getFullYear(),
        ].join('/');
    }

    const formatHour = (date: Date) => {
        return [
            padTo2Digits(date.getHours()),
            padTo2Digits(date.getMinutes())
        ].join(':');
    }

    return (
        <>
            <header>
                <nav>
                    <h1>Avistamentos</h1>
                    <Link to='/home'>Início</Link>| {'  '}
                    <Link to='/listaPassaro'>Buscar Pássaros</Link>| {' '}
                    <Link to='/avistamento'>Cadastro de Avistamento</Link>
                </nav>
            </header>
            <main>
                <div className="center">
                    <table>
                        <thead>
                            <tr>
                                <td>Local</td>
                                <td>Data</td>
                                <td>Hora</td>
                                <td>Página</td>
                                <td>Linha</td>
                                <td>Coluna</td>
                                <td>Pássaro</td>
                            </tr>
                        </thead>
                        <tbody>
                            {listaAvistamentos?.map(avistamento =>
                                <tr key={avistamento._id}>
                                    <td>{avistamento.local}</td>
                                    <td>{formatDate(new Date(avistamento.data))}</td>
                                    <td>{formatHour(new Date(avistamento.hora))}</td>
                                    <td>{avistamento.pagina}</td>
                                    <td>{avistamento.linha}</td>
                                    <td>{avistamento.coluna}</td>
                                    <td>{avistamento.passaro.nome}</td>
                                </tr>
                            )}
                        </tbody>
                    </table>
                </div>
            </main>
            <footer>
                <p>Desenvolvido por Rhanna e Micheli</p>
            </footer>
        </>
    );
}