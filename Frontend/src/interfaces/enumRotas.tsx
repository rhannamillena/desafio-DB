export enum EnumRotas {
    Nome = "buscarpornome",
    Habitat = "buscarporhabitat",
    Ingles = "buscarpornomeingles",
    Família = "buscarporfamilia",
    Tamanho = "buscarportamanho",
    Científico = "buscarpornomecientifico",
}