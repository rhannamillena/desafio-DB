export interface passaroInterface {
    _id: string;
    nome: string;
    nomeIngles: string;
    nomeCientifico: string;
    familia: string;
    tamanho: number;
    habitat: string;
}
