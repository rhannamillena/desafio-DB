import * as express from 'express';
import PassaroControler from '../backend/controladores/passaro_controler';

const passaroControler = new PassaroControler();

class Routes{
    routes: express.Router;

    constructor(){
        this.routes = express.Router();
        this.addRotas();
    }
    addRotas(){
        this.routes.get('/buscar', passaroControler.buscar.bind(passaroControler));
        this.routes.get('/buscarpornome/:nome', passaroControler.buscarPorNome.bind(passaroControler));
        this.routes.get('/buscarporhabitat/:habitat', passaroControler.buscarPorHabitat.bind(passaroControler));
        this.routes.get('/buscarpornomeingles/:nomeIngles', passaroControler.buscarPorNomeIngles.bind(passaroControler));
        this.routes.get('/buscarporfamilia/:familia', passaroControler.buscarPorFamilia.bind(passaroControler));
        this.routes.get('/buscarportamanho/:tamanho', passaroControler.buscarPorTamanho.bind(passaroControler));
        this.routes.get('/buscarpornomecientifico/:nomeCientifico', passaroControler.buscarPorNomeCientifico.bind(passaroControler));


    }

}

export default new Routes().routes
