import express from 'express';
import routes from '../backend/routes';
import { router as passaroRouter } from './routes/passaroRoute';
import { router as avistamentoRouter } from './routes/avistamentoRoute';
import database from './configs/database';
import cors from 'cors';


//import {createLogger} from 'winston';
const app = express();
const porta = 8081;
database();
app.use(cors({origin: '*'}));
app.use(express.json());
app.use('/passaro', routes);
app.use(avistamentoRouter);
app.use(passaroRouter);
app.listen(porta, () => {console.log(`Executando na porta, ${porta}`)})
