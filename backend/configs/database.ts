import mongoose from "mongoose";
import seedAves from "../seeder/seedAves";

const urlMongoDB = process.env.MONGO_URL || 'mongodb://localhost:27017'

export default () => {
    mongoose.connect(urlMongoDB);
    mongoose.connection.on('connected', () => { console.log('conectado na base') });
    seedAves();
}