import { model, Schema, SchemaTypes } from "mongoose";
import { Avistamento } from "../interfaces/avistamento";

const AvistamentoSchema = new Schema<Avistamento>({
    local: { type: String, required: true, minlength: 1, maxlength: 100 },
    data: { type: Date, required: true, minlength: 1, maxlength: 100 },
    hora: { type: Date, required: true },
    pagina: { type: Number, required: false },
    linha: { type: Number, required: false },
    coluna: { type: Number, required: false },
    passaro: { type: SchemaTypes.ObjectId, ref: 'Passaro' }
});

export const AvistamentoModel = model<Avistamento>('Avistamento', AvistamentoSchema, 'avistamentos');