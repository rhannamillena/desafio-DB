
import { model, Schema } from "mongoose";
import { Passaro } from "../interfaces/passaro_Interface";


const PassaroSchema = new Schema<Passaro>({
    nome: { type: String, required: true, minlength: 1, maxlength: 100 },
    nomeIngles: { type: String, required: true, minlength: 1, maxlength: 100 },
    nomeCientifico: { type: String, required: true, minlength: 1, maxlength: 100 },
    tamanho: { type: String, required: true, min: 0, max: 100 },
    genero: { type: String, required: false, minlength: 0, maxlength: 100 },
    familia: { type: String, required: true, minlength: 1, maxlength: 100 },
    habitat: { type: String, required: false, minlength: 0, maxlength: 100 },
    cor: { type: String, required: true, minlength: 1, maxlength: 100 },
});

export const PassaroModel = model<Passaro>('Passaro', PassaroSchema, 'passaros');