import { Passaro } from '../interfaces/passaro_Interface'; 
import { PassaroModel } from '../persistencia/passaroModel';

export async function criar(passaro: Passaro): Promise<Passaro> {
    return PassaroModel.create(passaro); 
}

export async function buscar(): Promise<Passaro[]> {
    let consulta = PassaroModel.find();
    return consulta.exec();
};

export async function BuscarPorNome(nome:String): Promise<Passaro[]> {
    let consulta = PassaroModel.where('nome', nome);
    return consulta.exec();
};

export async function BuscarPorHabitat(habitat:String): Promise<Passaro[]> {
    let consulta = PassaroModel.where('habitat', habitat);
    return consulta.exec();

};

export async function BuscarPorNomeIngles(nomeIngles:String): Promise<Passaro[]> {
    let consulta = PassaroModel.where('nomeIngles', nomeIngles);
    return consulta.exec();

};

export async function BuscarPorFamilia(familia:String): Promise<Passaro[]> {
    let consulta = PassaroModel.where('familia', familia);
    return consulta.exec();

}