import * as AvistamentoService from '../servicos/avistamento_service';
import * as PassaroService from '../servicos/passaro_service';
import baseDeDados from '../helper/tests/mongodb_helper';
import seedAves from '../seeder/seedAves';

describe('AvistamentoService', () => {
    beforeAll(async () => {
        await baseDeDados.abrir();
        await seedAves();
    });

    afterAll(async () => {
        await baseDeDados.fechar();
    });

    afterEach(async () => {
        await baseDeDados.limpar();
    });

    describe('buscar()', () => {
        test('deve retornar todos os avistamentos', async () => {
            const avistamentos = await AvistamentoService.buscar();
            expect(Array.isArray(avistamentos)).toBe(true);
            expect(avistamentos.length).toBe(0);
            expect(avistamentos).not.toBeNull();
        });
    });

    describe('criar()', () => {
        test('deve criar um novo avistamento', async () => {
            const passaros = await PassaroService.buscar();

            const avistamento = await AvistamentoService.criar({
                local: 'Porto Alegre',
                data: new Date('2022-11-01T20:14:00-03:00'),
                hora: new Date('2022-11-01T20:14:00-03:00'),
                pagina: 0,
                linha: 0,
                coluna: 0,
                passaro: passaros[0]
            });

            expect(avistamento?.local).toBe('Porto Alegre');
            expect(new Date(avistamento?.data).valueOf()).toBe(new Date('2022-11-01T20:14:00-03:00').valueOf());
            expect(new Date(avistamento?.hora).valueOf()).toBe(new Date('2022-11-01T20:14:00-03:00').valueOf());
            expect(avistamento?.pagina).toBe(0);
            expect(avistamento?.linha).toBe(0);
            expect(avistamento?.coluna).toBe(0);
            expect(avistamento?.passaro).toBe(passaros[0]);
        });
    });
});
