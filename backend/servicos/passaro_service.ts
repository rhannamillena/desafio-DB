import { PassaroModel } from "../persistencia/passaroModel";
import { Passaro } from '../interfaces/passaro_Interface';

export async function criar(passaro: Passaro): Promise<Passaro> {
    return PassaroModel.create(passaro); 
}

export async function buscar(): Promise<Passaro[]> {
    let consulta = PassaroModel.find({order: {createAt: 'DESC'}});
    return consulta.exec();
};

export async function buscarPorNome(nome:String): Promise<Passaro[]> {
    return PassaroModel.find({'nome': nome}).then(passaro => {
        if (passaro === null ){
            throw new Error()
        }
        return passaro;
    }).catch(error => {
        throw error
    })
    
};


export async function buscarPorHabitat(habitat:String): Promise<Passaro[]> {
    return PassaroModel.find({'habitat': habitat}).then(passaro => {
        if(passaro === null ){
            throw new Error()
        }
        return passaro;
    });
    
};

export async function buscarPorNomeIngles(nomeIngles:String): Promise<Passaro[]> {
    return PassaroModel.find({'nomeIngles': nomeIngles}).then(passaro => {
        if(passaro === null ){
            throw new Error()
        }
        return passaro;
    });
};
export async function buscarPorFamilia(familia:String): Promise<Passaro[]> {
    return PassaroModel.find({'familia': familia}).then(passaro => {
        if(passaro === null ){
            throw new Error()
        }
        return passaro;
    })
};
export async function buscarPorTamanho(tamanho:String): Promise<Passaro[]> {
    return PassaroModel.find({'tamanho': tamanho}).then(passaro => {
        if(passaro === null){
            throw new Error()
        }
        return passaro;
    })
    
}
export async function buscarPorNomeCientifico(nomeCientifico:String): Promise<Passaro[]> {
    return PassaroModel.find({'nomeCientifico' :nomeCientifico}).then(passaro => {
        if(passaro === null){
            throw new Error()
        }
        return passaro;
    })
}