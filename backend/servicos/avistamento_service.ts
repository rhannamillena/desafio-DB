import { AvistamentoModel } from "../persistencia/avistamentoModel";
import { Avistamento } from '../interfaces/avistamento';

export async function buscar(): Promise<Avistamento[]> {
    let consulta = AvistamentoModel.find().populate('passaro');
    return consulta.exec();
};

export async function criar(avistamento: Avistamento): Promise<Avistamento> {
    return AvistamentoModel.create(avistamento);
}
