import { Router } from 'express';
import AvistamentoController from '../controladores/avistamento_controller';

const avistamentoController = new AvistamentoController();
const router = Router();
const path = "/avistamento";

router.get(`${path}/buscar`, avistamentoController.buscar.bind(avistamentoController));
router.post(path, avistamentoController.criar.bind(avistamentoController));

export { router, path };