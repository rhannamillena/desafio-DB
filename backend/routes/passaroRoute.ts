import { Router } from 'express';
import PassaroControler from '../controladores/passaro_controler';

const passaroControler = new PassaroControler();
const router = Router();
const path = "/passaro";

router.get(`${path}/buscar`, passaroControler.buscar.bind(passaroControler));
router.get(`${path}/buscarpornome/:nome`, passaroControler.buscarPorNome.bind(passaroControler));
router.get(`${path}/buscarporhabitat/:habitat`, passaroControler.buscarPorHabitat.bind(passaroControler));
router.get(`${path}/buscarpornomeingles/:nomeIngles`, passaroControler.buscarPorNomeIngles.bind(passaroControler));
router.get(`${path}/buscarporfamilia/:familia`, passaroControler.buscarPorFamilia.bind(passaroControler));
router.get(`${path}/buscarportamanho/:tamanho`, passaroControler.buscarPorTamanho.bind(passaroControler));
router.get(`${path}/buscarpornomecientifico/:nomeCientifico`, passaroControler.buscarPorNomeCientifico.bind(passaroControler));

export { router, path };