import { Request, Response } from 'express';
import { buscar, buscarPorFamilia, buscarPorHabitat, buscarPorNome, buscarPorNomeIngles, buscarPorTamanho, buscarPorNomeCientifico } from '../servicos/passaro_service';


export default class PassaroControler {
    
    buscar(requisicao: Request, resposta: Response) {
        console.log("conectado ")
        buscar().then(passaro => {
            resposta.status(200).json(passaro);
        }).catch(erro => {
            console.log(erro)
            resposta.status(400).json({ erro });
        })
    }

    buscarPorNome(requisicao: Request, resposta: Response) {
        
        buscarPorNome(requisicao.params.nome).then(passaro => {
            resposta.status(200).json(passaro);
        }).catch(erro => {
            console.log(erro)
            resposta.status(400).json(('Pássaro não encontrado'));
        })

    }

    buscarPorHabitat(requisicao: Request, resposta: Response) {
        buscarPorHabitat(requisicao.params.habitat).then(passaro => {
            resposta.status(200).json(passaro);
        }).catch(erro => {
            console.log(erro)
            resposta.status(400).json(('Pássaro não encontrado'));
        })
    }

    buscarPorNomeIngles(requisicao: Request, resposta: Response) {
        buscarPorNomeIngles(requisicao.params.nomeIngles).then(passaro => {
            resposta.status(200).json(passaro);
        }).catch(erro => {
            console.log(erro)
            resposta.status(400).json(('Pássaro não encontrado'))
        })
    }

    buscarPorFamilia(requisicao: Request, resposta: Response) {
        buscarPorFamilia(requisicao.params.familia).then(passaro => {
            resposta.status(200).json(passaro);
        }).catch(erro => {
            console.log(erro)
            resposta.status(400).json(('Pássaro não encontrado'))
        })
    }

    buscarPorTamanho(requisicao: Request, resposta: Response) {
        buscarPorTamanho(requisicao.params.tamanho).then((passaro) => {
            resposta.status(200).json(passaro);
        }).catch(erro => {
            console.log(erro)
            resposta.status(400).json(('Pássaro não encontrado'))
        })
    }

    buscarPorNomeCientifico(requisicao: Request, resposta: Response) {
        buscarPorNomeCientifico(requisicao.params.nomeCientifico).then((passaro) => {
            resposta.status(200).json(passaro);
        }).catch(erro => {
            console.log(erro)
            resposta.status(400).json(('Pássaro não encontrado'))
        })
    }
}
