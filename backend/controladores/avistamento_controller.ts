import { Request, Response } from 'express';
import { buscar, criar } from '../servicos/avistamento_service';


export default class AvistamentoController {
    buscar(requisicao: Request, resposta: Response) {
        buscar().then(avistamentos => {
            resposta.status(200).json(avistamentos);
        }).catch(erro => {
            console.log(erro)
            resposta.status(400).json({ erro });
        })
    }

    criar(requisicao: Request, resposta: Response) {
        const { local, data, hora, pagina, linha, coluna, passaro } = requisicao.body;

        if (local && data && hora && passaro) {
            let avistamento = {
                local: local,
                data: new Date(data),
                hora: new Date(hora),
                pagina: parseInt(pagina || 0),
                linha: parseInt(linha || 0),
                coluna: parseInt(coluna || 0),
                passaro: passaro
            }

            criar(avistamento).then(avistamento => {
                resposta.status(200).json(avistamento);
            }).catch(erro => {
                console.log(erro)
                resposta.status(400).json({ erro });
            })
        } else {
            resposta.status(400).json({ message: 'Campos obrigatórios não preenchidos' });
        }
    }
}
