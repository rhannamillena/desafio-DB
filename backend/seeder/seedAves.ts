import mongoose from 'mongoose';
import { PassaroModel } from '../persistencia/passaroModel';

const passaros = [
    new PassaroModel({
        nome: 'joão-grande',
        nomeIngles: 'Maguari Stork',
        nomeCientifico: 'Ciconia maguari',
        tamanho: '85',
        genero: '',
        familia: 'Ciconiidae',
        habitat: 'campo alagado, banhado',
        cor: 'Branco'
    }),
    new PassaroModel({
        nome: 'cabeça-seca',
        nomeIngles: 'Wood Stork',
        nomeCientifico: 'Mycteria americana',
        tamanho: '65',
        genero: '',
        familia: 'Ciconiidae',
        habitat: 'campo alagado, banhado',
        cor: 'Branco'
    }),
    new PassaroModel({
        nome: 'cisne-de-pescoço-preto',
        nomeIngles: 'Black-necked-swan',
        nomeCientifico: 'Cygnus melancoryphus',
        tamanho: '80',
        genero: '',
        familia: 'Anatidae',
        habitat: '',
        cor: 'Branco'
    }),
    new PassaroModel({
        nome: 'capororoca',
        nomeIngles: 'Coscoroba Swan',
        nomeCientifico: 'Cosocoroba coscoroba',
        tamanho: '65',
        genero: '',
        familia: 'Anatidae',
        habitat: 'campo alagado, banhado',
        cor: 'Branco'
    }),
    new PassaroModel({
        nome: 'garça-branca-grande',
        nomeIngles: 'Great Egret',
        nomeCientifico: 'Ardea alba',
        tamanho: '65',
        genero: '',
        familia: 'Ardeidae',
        habitat: 'campo alagado, banhado',
        cor: 'Branco'
    }),
    new PassaroModel({
        nome: 'garça-branca-pequena',
        nomeIngles: 'Snowy Egret',
        nomeCientifico: 'Egretta thula',
        tamanho: '40',
        genero: '',
        familia: 'Ardeidae',
        habitat: 'campo alagado, banhado',
        cor: 'Branco'
    }),
    new PassaroModel({
        nome: 'garça-vaqueira',
        nomeIngles: 'Cattle Egret',
        nomeCientifico: 'Bubulcus ibis',
        tamanho: '35',
        genero: '',
        familia: 'Ardeidae',
        habitat: 'campo seco baixo, campo alagado',
        cor: 'Branco'
    }),
    new PassaroModel({
        nome: 'águia-pescadora',
        nomeIngles: 'Osprey',
        nomeCientifico: 'Pandion haliaetus',
        tamanho: '57',
        genero: '',
        familia: 'Pandionidae',
        habitat: '',
        cor: 'Branco'
    }),
    new PassaroModel({
        nome: 'gavião-de-rabo-branco',
        nomeIngles: 'White-tailed Hawk',
        nomeCientifico: 'Geranoaetus albicaudatus',
        tamanho: '48-58',
        genero: '',
        familia: 'Accipitridae',
        habitat: '',
        cor: 'Branco'
    }),
    new PassaroModel({
        nome: 'gavião-peneira',
        nomeIngles: 'White-tailed Kite',
        nomeCientifico: 'Elanus leucurus',
        tamanho: '35',
        genero: '',
        familia: 'Accipitridae',
        habitat: 'campo alagado, banhado com vegetação alta',
        cor: 'Branco'
    }),
    new PassaroModel({
        nome: 'gavião-peneira',
        nomeIngles: 'White-tailed Kite',
        nomeCientifico: 'Elanus leucurus',
        tamanho: '35',
        genero: '',
        familia: 'Accipitridae',
        habitat: '',
        cor: 'Branco'
    }),
    new PassaroModel({
        nome: 'gaivota-de-cabeça-cinza',
        nomeIngles: 'Grey-headed Gull',
        nomeCientifico: 'Chroicocephalus cirrocephalus',
        tamanho: '38',
        genero: '',
        familia: 'Laridae',
        habitat: 'campo alagado, banhado com vegetação alta, banhado',
        cor: 'Branco'
    }),
    new PassaroModel({
        nome: 'gaivota-maria-velha',
        nomeIngles: 'Brown-hooded Gull',
        nomeCientifico: 'Chroicocephalus maculipennis',
        tamanho: '35',
        genero: '',
        familia: 'Laridae',
        habitat: 'campo alagado, banhado com vegetação alta, banhado',
        cor: 'Branco'
    }),
    new PassaroModel({
        nome: 'pernilongo-de-costas-brancas',
        nomeIngles: 'White-necked-Stilt',
        nomeCientifico: 'Himantopus melanurus',
        tamanho: '35',
        genero: '',
        familia: 'Laridae',
        habitat: 'campo alagado, banhado',
        cor: 'Branco'
    }),
    new PassaroModel({
        nome: 'trinta-réis-anão',
        nomeIngles: 'Yellow-Billed Tern',
        nomeCientifico: 'Sternula superciliaris',
        tamanho: '22',
        genero: '',
        familia: 'Sternidae',
        habitat: 'campo seco baixo, banhado',
        cor: 'Branco'
    }),
    new PassaroModel({
        nome: 'pica-pau-branco',
        nomeIngles: 'White Woodpecker',
        nomeCientifico: 'Melanerpes candidus',
        tamanho: '24-29',
        genero: '',
        familia: 'Picidae',
        habitat: 'campo com árvores',
        cor: 'Branco'
    }),
    new PassaroModel({
        nome: 'tesourinha',
        nomeIngles: 'Fork-tailed Flycatcher',
        nomeCientifico: 'Tyrannus savana',
        tamanho: '28-38',
        genero: '',
        familia: 'Tyrannidae',
        habitat: 'campo seco alto, campo com árvores',
        cor: 'Branco'
    }),
    new PassaroModel({
        nome: 'noivinha-coroada',
        nomeIngles: 'Black-crowned Monjita',
        nomeCientifico: 'Xolmis coronatus',
        tamanho: '20',
        genero: '',
        familia: 'Tyrannidae',
        habitat: 'campo com árvores',
        cor: 'Branco'
    }),
    new PassaroModel({
        nome: 'noivinha-de-rabo-preto',
        nomeIngles: 'Black-and-white Monjita',
        nomeCientifico: 'Xolmis dominicanus',
        tamanho: '19',
        genero: 'Macho',
        familia: 'Tyrannidae',
        habitat: 'campo seco baixo, campo seco alto',
        cor: 'Branco'
    }),
    new PassaroModel({
        nome: 'noivinha-de-rabo-preto',
        nomeIngles: 'Black-and-white Monjita',
        nomeCientifico: 'Xolmis dominicanus',
        tamanho: '19',
        genero: 'Fêmea',
        familia: 'Tyrannidae',
        habitat: 'campo seco baixo, campo seco alto',
        cor: 'Branco'
    }),
    new PassaroModel({
        nome: 'noivinha',
        nomeIngles: 'White Monjita',
        nomeCientifico: 'Xolmis irupero',
        tamanho: '17',
        genero: '',
        familia: 'Tyrannidae',
        habitat: 'campo seco baixo, campo com árvores',
        cor: 'Branco'
    }),
    new PassaroModel({
        nome: 'cardeal',
        nomeIngles: 'Red-crested Cardinal',
        nomeCientifico: 'Paroaria coronata',
        tamanho: '17',
        genero: '',
        familia: 'Thraupidae',
        habitat: 'campo com árvores',
        cor: 'Branco'
    }),
    new PassaroModel({
        nome: 'capacetinho',
        nomeIngles: 'Black-capped Warbling-Finch',
        nomeCientifico: 'Poospiza melanoleuca',
        tamanho: '13',
        genero: 'Fêmea',
        familia: 'Thraupidae',
        habitat: 'campo seco alto, campo com árvores',
        cor: 'Branco'
    }),
    new PassaroModel({
        nome: 'tijerila',
        nomeIngles: 'White-naped Xenopsaris',
        nomeCientifico: 'Xenopsaris albinucha',
        tamanho: '13',
        genero: 'Macho',
        familia: 'Tityridae',
        habitat: 'campo com árvores',
        cor: 'Branco'
    }),
    new PassaroModel({
        nome: 'urubu-de-cabeça-vermelha',
        nomeIngles: 'Turkey Vulture',
        nomeCientifico: 'Cathartes aura',
        tamanho: '55',
        genero: '',
        familia: 'Cathartidae',
        habitat: 'campo seco baixo, campo seco alto, campo com árvores',
        cor: 'Preto'
    }),
    new PassaroModel({
        nome: 'urubu-de-cabeça-preta',
        nomeIngles: 'Black Vulture',
        nomeCientifico: 'Coragyps atratus',
        tamanho: '53',
        genero: '',
        familia: 'Cathartidae',
        habitat: '',
        cor: 'Preto'
    }),
    new PassaroModel({
        nome: 'urubu-de-cabeça-amarela',
        nomeIngles: 'Lesser Yellow-headed Vulture',
        nomeCientifico: 'Cathartes burrovianus',
        tamanho: '51',
        genero: '',
        familia: 'Cathartidae',
        habitat: 'campo seco baixo, campo seco alto, campo com árvores, campo alagado, banhado com vegetação alta',
        cor: 'Preto'
    }),
    new PassaroModel({
        nome: 'maçarico-preto',
        nomeIngles: 'White-faced Ibis',
        nomeCientifico: 'Plegadis chihi',
        tamanho: '40',
        genero: '',
        familia: 'Threskiornithidae',
        habitat: 'campo alagado, banhado',
        cor: 'Preto'
    }),
    new PassaroModel({
        nome: 'maçarico-de-cara-pelada',
        nomeIngles: 'Whispering Ibis',
        nomeCientifico: 'Phimosus infuscatus',
        tamanho: '40',
        genero: '',
        familia: 'Threskiornithidae',
        habitat: 'campo alagado, banhado',
        cor: 'Preto'
    }),
    new PassaroModel({
        nome: 'águia-chilena',
        nomeIngles: 'Black-chested Buzzard-Eagle',
        nomeCientifico: 'Geranoaetus melanoleucus',
        tamanho: '60-70',
        genero: 'Jovem',
        familia: 'Accipitridae',
        habitat: '',
        cor: 'Preto'
    }),
    new PassaroModel({
        nome: 'gavião-caramujeiro',
        nomeIngles: 'Snail Kite',
        nomeCientifico: 'Rostrhamus sociabilis',
        tamanho: '38',
        genero: 'Fêmea',
        familia: 'Accipitridae',
        habitat: 'campo alagado, banhado com vegetação alta, banhado',
        cor: 'Preto'
    }),
    new PassaroModel({
        nome: 'gavião-caramujeiro',
        nomeIngles: 'Snail Kite',
        nomeCientifico: 'Rostrhamus sociabilis',
        tamanho: '38',
        genero: 'Macho',
        familia: 'Accipitridae',
        habitat: 'campo alagado, banhado com vegetação alta, banhado',
        cor: 'Preto'
    }),
    new PassaroModel({
        nome: 'anu-preto',
        nomeIngles: 'Crotophaga ani',
        nomeCientifico: 'Smooth-billed Ani',
        tamanho: '32',
        genero: '',
        familia: 'Cuculidae',
        habitat: '',
        cor: 'Preto'
    }),
    new PassaroModel({
        nome: 'graúna',
        nomeIngles: 'Chopi Blackbird',
        nomeCientifico: 'Gnorinopsar chop',
        tamanho: '23',
        genero: '',
        familia: 'Icteridae',
        habitat: 'campo seco baixo',
        cor: 'Preto'
    }),
    new PassaroModel({
        nome: 'vira-bosta',
        nomeIngles: 'Shiny Cowbird,',
        nomeCientifico: 'Molothrus bonariensis',
        tamanho: '19',
        genero: 'Macho',
        familia: 'Icteridae',
        habitat: 'campo seco baixo, campo com árvores',
        cor: 'Preto'
    }),
    new PassaroModel({
        nome: 'chopim-do-brejo',
        nomeIngles: 'Yellow-rumped Marshbird',
        nomeCientifico: 'Pseudoleistes guirahuro',
        tamanho: '22',
        genero: '',
        familia: 'Icteridae',
        habitat: 'campo seco baixo, campo com árvores, banhado com vegetação alta, campo alagado',
        cor: 'Preto'
    }),
    new PassaroModel({
        nome: 'Dragão',
        nomeIngles: 'Brown-and-yellow Marshbird',
        nomeCientifico: 'Pseudoleistes virescens',
        tamanho: '21',
        genero: '',
        familia: 'Icteridae',
        habitat: 'campo seco baixo, campo com árvores, banhado com vegetação alta, campo alagado',
        cor: 'Preto'
    }),
    new PassaroModel({
        nome: 'Veste-amarela',
        nomeIngles: 'Saffron-cowled Blackbird',
        nomeCientifico: 'Xanthopsar flavus',
        tamanho: '19',
        genero: 'Macho',
        familia: 'Icteridae',
        habitat: 'campo seco baixo, banhado com vegetação alta',
        cor: 'Preto'
    }),
    new PassaroModel({
        nome: 'Veste-amarela',
        nomeIngles: 'Saffron-cowled Blackbird',
        nomeCientifico: 'Xanthopsar flavus',
        tamanho: '19',
        genero: 'Fêmea',
        familia: 'Icteridae',
        habitat: 'campo seco baixo, banhado com vegetação alta',
        cor: 'Preto'
    }),
    new PassaroModel({
        nome: 'Cardeal-do-banhado',
        nomeIngles: 'Scarlet-headed Blackbird',
        nomeCientifico: 'Amblyramphus holosericeus',
        tamanho: '22',
        genero: '',
        familia: 'Icteridae',
        habitat: 'banhado com vegetação alta',
        cor: 'Preto'
    }),
    new PassaroModel({
        nome: 'Polícia-inglesa',
        nomeIngles: 'White-browed Blackbird',
        nomeCientifico: 'Sturnella superciliaris',
        tamanho: '17',
        genero: 'Macho',
        familia: 'Icteridae',
        habitat: 'campo seco alto, campo alagado',
        cor: 'Preto'
    }),
    new PassaroModel({
        nome: 'Garibaldi',
        nomeIngles: 'Chestnut-capped Blackbird',
        nomeCientifico: 'Chrysomus ruficapillus',
        tamanho: '17',
        genero: 'Macho',
        familia: 'Icteridae',
        habitat: '',
        cor: 'Preto'
    }),
    new PassaroModel({
        nome: 'Sargento',
        nomeIngles: 'Yellow-winged Blackbird',
        nomeCientifico: 'Agelasticus thilius',
        tamanho: '17',
        genero: 'Macho',
        familia: 'Icteridae',
        habitat: 'banhado com vegetação alta',
        cor: 'Preto'
    }),
    new PassaroModel({
        nome: 'vira-bosta-picumã',
        nomeIngles: 'Yellow-winged Blackbird',
        nomeCientifico: 'Screaming Cowbird',
        tamanho: '18',
        genero: '',
        familia: 'Icteridae',
        habitat: '',
        cor: 'Preto'
    }),
    new PassaroModel({
        nome: 'maria-preta-de-penacho',
        nomeIngles: 'Crested Black-Tyrant',
        nomeCientifico: 'Knipolegus lophotes',
        tamanho: '19',
        genero: '',
        familia: 'Tyrannidae',
        habitat: '',
        cor: 'Preto'
    }),
    new PassaroModel({
        nome: 'príncipe',
        nomeIngles: 'Vermilion Flycatcher',
        nomeCientifico: 'Pyrocephalus rubinus',
        tamanho: '13',
        genero: 'Macho',
        familia: 'Tyrannidae',
        habitat: 'campo seco baixo, campo com árores',
        cor: 'Preto'
    }),
    new PassaroModel({
        nome: 'viuvinha-de-óculos',
        nomeIngles: 'Spectacled Tyrant',
        nomeCientifico: 'Hymenops perspicillatus',
        tamanho: '13',
        genero: 'Macho',
        familia: 'Tyrannidae',
        habitat: 'campo seco alto, campo alagado, banhado',
        cor: 'Preto'
    }),
    new PassaroModel({
        nome: 'colegial',
        nomeIngles: 'Patagonian Negrito',
        nomeCientifico: 'Lessonia ruf',
        tamanho: '11',
        genero: 'Macho',
        familia: 'Tyrannidae',
        habitat: 'campo seco baixo, campo alagado',
        cor: 'Preto'
    }),
    new PassaroModel({
        nome: 'freirinha',
        nomeIngles: 'White-headed Marsh Tyrant',
        nomeCientifico: 'Arundinicola leucocephala',
        tamanho: '12',
        genero: 'Macho',
        familia: 'Tyrannidae',
        habitat: '',
        cor: 'Preto'
    }),
    new PassaroModel({
        nome: 'andorinha-doméstica-grande',
        nomeIngles: 'Gray-breasted Martin',
        nomeCientifico: 'Progne chalybea',
        tamanho: '18',
        genero: '',
        familia: 'Hirundinidae',
        habitat: '',
        cor: 'Preto'
    }),
    new PassaroModel({
        nome: 'andorinha-de-testa-branca',
        nomeIngles: 'White-rumped Swallow',
        nomeCientifico: 'Tachycineta leucorrhoa',
        tamanho: '13',
        genero: '',
        familia: 'Hirundinidae',
        habitat: '',
        cor: 'Preto'
    }),
    new PassaroModel({
        nome: 'andorinha-pequena-de-casa',
        nomeIngles: 'Blue-and-white Swallow',
        nomeCientifico: 'Pygochelidon cyanoleuca',
        tamanho: '11',
        genero: '',
        familia: 'Hirundinidae',
        habitat: '',
        cor: 'Preto'
    }),
    new PassaroModel({
        nome: 'tiziu',
        nomeIngles: 'Blue-black Grassquit',
        nomeCientifico: 'Volatinia jacarina',
        tamanho: '9',
        genero: 'Macho',
        familia: 'Thraupidae',
        habitat: 'campo com árores',
        cor: 'Preto'
    }),
    new PassaroModel({
        nome: 'ema',
        nomeIngles: 'Greater Rhea',
        nomeCientifico: 'Rhea americana',
        tamanho: '130-150',
        genero: '',
        familia: 'Rheidae',
        habitat: 'campo seco baixo, campo seco alto',
        cor: 'Cinza'
    }),
    new PassaroModel({
        nome: 'tachã',
        nomeIngles: 'Southern Screamer',
        nomeCientifico: 'Chauna torquata',
        tamanho: '85',
        genero: '',
        familia: 'Anhimidae',
        habitat: 'campo alagado, banhado',
        cor: 'Cinza'
    }),
    new PassaroModel({
        nome: 'seriema',
        nomeIngles: 'Red-legged Seriema',
        nomeCientifico: 'Cariama cristata',
        tamanho: '70',
        genero: '',
        familia: 'Cariamidae',
        habitat: 'campo seco baixo, campo seco alto, campo com árvores, campo alagado',
        cor: 'Cinza'
    }),
    new PassaroModel({
        nome: 'garça-moura',
        nomeIngles: 'Cocoi Heron',
        nomeCientifico: 'Ardea cocoi',
        tamanho: '75',
        genero: '',
        familia: 'Ardeidae',
        habitat: 'campo alagado, banhado',
        cor: 'Cinza'
    }),
    new PassaroModel({
        nome: 'maria-faceira',
        nomeIngles: 'Whistling Heron',
        nomeCientifico: 'Syrigma sibilatrix',
        tamanho: '48',
        genero: '',
        familia: 'Ardeidae',
        habitat: 'campo alagado, banhado',
        cor: 'Cinza'
    }),
    new PassaroModel({
        nome: 'socozinho',
        nomeIngles: 'Striated Heron',
        nomeCientifico: 'Butorides striat',
        tamanho: '34',
        genero: '',
        familia: 'Ardeidae',
        habitat: 'campo alagado, banhado com vegetação alta',
        cor: 'Cinza'
    }),
    new PassaroModel({
        nome: 'maçarico-realsocozinho',
        nomeIngles: 'Plumbeous Ibis',
        nomeCientifico: 'Theristicus caerulescens',
        tamanho: '62',
        genero: '',
        familia: 'Threskiornithidae',
        habitat: 'campo alagado, banhado',
        cor: 'Cinza'
    }),
    new PassaroModel({
        nome: 'curicaca',
        nomeIngles: 'Buff-necked Ibis',
        nomeCientifico: 'Theristicus caudatus',
        tamanho: '57',
        genero: '',
        familia: 'Threskiornithidae',
        habitat: 'campo seco baixo, campo alagado',
        cor: 'Cinza'
    }),
    new PassaroModel({
        nome: 'quero-quero',
        nomeIngles: 'Southern Lapwing',
        nomeCientifico: 'Vanellus chilensis',
        tamanho: '31',
        genero: '',
        familia: 'Charadriidae',
        habitat: 'campo seco baixo, campo alagado',
        cor: 'Cinza'
    }),
    new PassaroModel({
        nome: 'batuíra-de-papo-ferrugíneo',
        nomeIngles: 'Tawny-throated Dotterelg',
        nomeCientifico: 'Oreopholus ruficollis',
        tamanho: '31',
        genero: '',
        familia: 'Charadriidae',
        habitat: 'campo seco baixo',
        cor: 'Cinza'
    }),
    new PassaroModel({
        nome: 'águia-cinzenta',
        nomeIngles: 'Crowned Eagle',
        nomeCientifico: 'Urubitinga coronat',
        tamanho: '62-72',
        genero: 'Jovem',
        familia: 'Accipitridae',
        habitat: 'campo seco alto, campo com árvores',
        cor: 'Cinza'
    }),
    new PassaroModel({
        nome: 'águia-chilena',
        nomeIngles: 'Black-chested Buzzard-Eagle',
        nomeCientifico: 'Geranoaetus melanoleucus',
        tamanho: '60-70',
        genero: '',
        familia: 'Accipitridae',
        habitat: 'campo seco baixo, campo com árvores',
        cor: 'Cinza'
    }),
    new PassaroModel({
        nome: 'gavião-do-banhado',
        nomeIngles: 'Long-winged Harrier',
        nomeCientifico: 'Circus buffon',
        tamanho: '50-55',
        genero: '',
        familia: 'Accipitridae',
        habitat: 'campo alagado, banhado com vegetação alta',
        cor: 'Cinza'
    }),
    new PassaroModel({
        nome: 'gavião-cinza',
        nomeIngles: 'Cinereous Harrier',
        nomeCientifico: 'Circus cinereus',
        tamanho: '40-48',
        genero: 'Macho',
        familia: 'Accipitridae',
        habitat: 'campo alagado, banhado com vegetação alta',
        cor: 'Cinza'
    }),
    new PassaroModel({
        nome: 'falcão-peregrino',
        nomeIngles: 'Peregrine Falcon',
        nomeCientifico: 'Falco peregrinus',
        tamanho: '37-42',
        genero: '',
        familia: 'Falconidae',
        habitat: '',
        cor: 'Cinza'
    }),
    new PassaroModel({
        nome: 'quiriquiri',
        nomeIngles: 'American Kestrel',
        nomeCientifico: 'Falco sparverius',
        tamanho: '25-28',
        genero: 'Fêmea',
        familia: 'Falconidae',
        habitat: '',
        cor: 'Cinza'
    }),
    new PassaroModel({
        nome: 'quiriquiri',
        nomeIngles: 'American Kestrel',
        nomeCientifico: 'Falco sparverius',
        tamanho: '25-28',
        genero: 'Macho',
        familia: 'Falconidae',
        habitat: '',
        cor: 'Cinza'
    }),
    new PassaroModel({
        nome: 'quiriquiri',
        nomeIngles: 'American Kestrel',
        nomeCientifico: 'Falco sparverius',
        tamanho: '25-28',
        genero: 'Jovem',
        familia: 'Falconidae',
        habitat: '',
        cor: 'Cinza'
    }),
    new PassaroModel({
        nome: 'pombão',
        nomeIngles: 'Picazuro Pigeon',
        nomeCientifico: 'Patagioenas picazuro',
        tamanho: '34',
        genero: '',
        familia: 'Columbidae',
        habitat: '',
        cor: 'Cinza'
    }),
    new PassaroModel({
        nome: 'pomba-do-orvalho',
        nomeIngles: 'Spot-winged Pigeon',
        nomeCientifico: 'Patagioenas maculosa',
        tamanho: '35',
        genero: '',
        familia: 'Columbidae',
        habitat: '',
        cor: 'Cinza'
    }),
    new PassaroModel({
        nome: 'pomba-de-bando',
        nomeIngles: 'Eared Dove',
        nomeCientifico: 'Zenaida auriculata',
        tamanho: '22',
        genero: '',
        familia: 'Columbidae',
        habitat: '',
        cor: 'Cinza'
    }),
    new PassaroModel({
        nome: 'rolinha-picuí',
        nomeIngles: 'Picui Ground-Dove',
        nomeCientifico: 'Columbina picui',
        tamanho: '15',
        genero: '',
        familia: 'Columbidae',
        habitat: 'campo seco alto, campo com árvores',
        cor: 'Cinza'
    }),
    new PassaroModel({
        nome: 'rolinha-picuí',
        nomeIngles: 'Streamer-tailed Tyrant',
        nomeCientifico: 'Gubernetes yetapa',
        tamanho: '40',
        genero: '',
        familia: 'Tyrannidae',
        habitat: 'campo seco alto, banhado com vegetação alta',
        cor: 'Cinza'
    }),
    new PassaroModel({
        nome: 'primavera',
        nomeIngles: 'Grey Monjita',
        nomeCientifico: 'Xolmis cinereus',
        tamanho: '40',
        genero: '',
        familia: 'Tyrannidae',
        habitat: 'campo seco alto, campo com árvores',
        cor: 'Cinza'
    }),
    new PassaroModel({
        nome: 'peitica-de-chapéu-preto',
        nomeIngles: 'Crowned Slaty Flycatcher',
        nomeCientifico: 'Griseotyrannus aurantioatrocristatus',
        tamanho: '18',
        genero: '',
        familia: 'Tyrannidae',
        habitat: 'campo com árvores',
        cor: 'Cinza'
    }),
    new PassaroModel({
        nome: 'andorinha-do-campo',
        nomeIngles: 'Brown-chested Martin',
        nomeCientifico: 'Progne tapera',
        tamanho: '16',
        genero: '',
        familia: 'Hirundinidae',
        habitat: '',
        cor: 'Cinza'
    }),
    new PassaroModel({
        nome: 'suiriri-cinzento',
        nomeIngles: 'Suiriri Flycatcher',
        nomeCientifico: 'Suiriri suiriri',
        tamanho: '13',
        genero: '',
        familia: 'Tyrannidae',
        habitat: '',
        cor: 'Cinza'
    }),
    new PassaroModel({
        nome: 'príncipe',
        nomeIngles: 'Vermilion Flycatcher',
        nomeCientifico: 'Pyrocephalus rubinus',
        tamanho: '13',
        genero: 'Fêmea',
        familia: 'Tyrannidae',
        habitat: 'campo seco baixo, campo com árvores',
        cor: 'Cinza'
    }),
    new PassaroModel({
        nome: 'joão-pobre',
        nomeIngles: 'Sooty Tyrannulet',
        nomeCientifico: 'Serpophaga nigricans',
        tamanho: '11-12',
        genero: '',
        familia: 'Tyrannidae',
        habitat: 'banhado com vegetação alta, banhado',
        cor: 'Cinza'
    }),
    new PassaroModel({
        nome: 'alegrinho',
        nomeIngles: 'White-crested Tyrannulet',
        nomeCientifico: 'Serpophaga subcristata',
        tamanho: '9',
        genero: '',
        familia: 'Tyrannidae',
        habitat: '',
        cor: 'Cinza'
    }),
    new PassaroModel({
        nome: 'balança-rabo-de-máscara',
        nomeIngles: 'Masked Gnatcatcher',
        nomeCientifico: 'Polioptila dumicola',
        tamanho: '12',
        genero: 'Macho',
        familia: 'Polioptilidae',
        habitat: 'campo com árvores',
        cor: 'Cinza'
    }),
    new PassaroModel({
        nome: 'balança-rabo-de-máscara',
        nomeIngles: 'Masked Gnatcatcher',
        nomeCientifico: 'Polioptila dumicola',
        tamanho: '12',
        genero: 'Fêmea',
        familia: 'Polioptilidae',
        habitat: '',
        cor: 'Cinza'
    }),
    new PassaroModel({
        nome: 'quem-te-vestiu',
        nomeIngles: 'Black-and-rufous Warbling-Finch',
        nomeCientifico: 'Poospiza nigrorufa',
        tamanho: '13',
        genero: 'Jovem',
        familia: 'Thraupidae',
        habitat: '',
        cor: 'Cinza'
    }),
    new PassaroModel({
        nome: 'patativa-tropeira',
        nomeIngles: 'Tropeiro Seedeater',
        nomeCientifico: 'Sporophila beltoni',
        tamanho: '12',
        genero: 'Macho',
        familia: 'Thraupidae',
        habitat: 'campo seco alto, campo com árvores',
        cor: 'Cinza'
    }),
    new PassaroModel({
        nome: 'coleirinho',
        nomeIngles: 'Double-collared Seedeater',
        nomeCientifico: 'Sporophila caerulescens',
        tamanho: '10',
        genero: 'Macho',
        familia: 'Thraupidae',
        habitat: 'campo seco alto, campo com árvores',
        cor: 'Cinza'
    }),
    new PassaroModel({
        nome: 'caboclinho-de-barriga-preta',
        nomeIngles: 'Black-bellied Seedeater',
        nomeCientifico: 'Sporophila melanogaster',
        tamanho: '10',
        genero: 'Macho',
        familia: 'Thraupidae',
        habitat: 'campo seco alto, campo com árvores',
        cor: 'Cinza'
    }),
    new PassaroModel({
        nome: 'socó-boi',
        nomeIngles: 'Rufescent Tiger-Heron',
        nomeCientifico: 'Tigrisoma lineatum',
        tamanho: '62',
        genero: '',
        familia: 'Ardeidae',
        habitat: 'campo alagado, banhado com vegetação alta',
        cor: 'Marrom'
    }),
    new PassaroModel({
        nome: 'carão',
        nomeIngles: 'Limpkin',
        nomeCientifico: 'Aramus guarauna',
        tamanho: '54',
        genero: '',
        familia: 'Ardeidae',
        habitat: 'campo alagado, banhado com vegetação alta',
        cor: 'Marrom'
    }),
    new PassaroModel({
        nome: 'marreca-piadeira ou irerê',
        nomeIngles: 'White-faced Whistling-Duck',
        nomeCientifico: 'Dendrocygna viduata',
        tamanho: '38',
        genero: '',
        familia: 'Anatidae',
        habitat: 'campo alagado, banhado',
        cor: 'Marrom'
    }),
    new PassaroModel({
        nome: 'marreca-parda',
        nomeIngles: 'Yellow-billed Pintail',
        nomeCientifico: 'Anas georgica',
        tamanho: '39',
        genero: '',
        familia: 'Anatidae',
        habitat: 'campo alagado, banhado',
        cor: 'Marrom'
    }),
    new PassaroModel({
        nome: 'marreca-pé-vermelho',
        nomeIngles: 'Brazilian Teal',
        nomeCientifico: 'Amazonetta brasiliensis',
        tamanho: '35',
        genero: 'Fêmea',
        familia: 'Anatidae',
        habitat: 'campo alagado, banhado',
        cor: 'Marrom'
    }),
    new PassaroModel({
        nome: 'marreca-pé-vermelho',
        nomeIngles: 'Brazilian Teal',
        nomeCientifico: 'Amazonetta brasiliensis',
        tamanho: '35',
        genero: 'Macho',
        familia: 'Anatidae',
        habitat: 'campo alagado, banhado',
        cor: 'Marrom'
    }),
    new PassaroModel({
        nome: 'marreca-pardinha',
        nomeIngles: 'Speckled Teal',
        nomeCientifico: 'Anas flavirostris',
        tamanho: '33',
        genero: '',
        familia: 'Anatidae',
        habitat: 'campo alagado, banhado',
        cor: 'Marrom'
    }),
    new PassaroModel({
        nome: 'gavião-caboclo',
        nomeIngles: 'Savanna Hawk',
        nomeCientifico: 'Heterospizias meridionalis',
        tamanho: '46-50',
        genero: 'Jovem',
        familia: 'Accipitridae',
        habitat: 'campo seco baixo, campo seco alto, campo com árvores, campo alagado, banhado com vegetação alta',
        cor: 'Marrom'
    }),
    new PassaroModel({
        nome: 'gavião-caboclo',
        nomeIngles: 'Savanna Hawk',
        nomeCientifico: 'Heterospizias meridionalis',
        tamanho: '46-50',
        genero: '',
        familia: 'Accipitridae',
        habitat: 'campo seco baixo, campo seco alto, campo com árvores, campo alagado, banhado com vegetação alta',
        cor: 'Marrom'
    }),
    new PassaroModel({
        nome: 'gavião-de-rabo-branco',
        nomeIngles: 'White-tailed Hawk',
        nomeCientifico: 'Geranoaetus albicaudatus',
        tamanho: '48-58',
        genero: 'Jovem',
        familia: 'Accipitridae',
        habitat: 'campo seco baixo, campo seco alto, campo com árvores, campo alagado, banhado com vegetação alta',
        cor: 'Marrom'
    }),
    new PassaroModel({
        nome: 'gavião-carijó',
        nomeIngles: 'Roadside Hawk',
        nomeCientifico: 'Rupornis magnirostris',
        tamanho: '34',
        genero: '',
        familia: 'Accipitridae',
        habitat: '',
        cor: 'Marrom'
    }),
    new PassaroModel({
        nome: 'caracará',
        nomeIngles: 'Southern Caracara',
        nomeCientifico: 'Caracara plancus',
        tamanho: '55',
        genero: '',
        familia: 'Falconidae',
        habitat: 'campo seco baixo, campo seco alto, campo com árvores, campo alagado',
        cor: 'Marrom'
    }),
    new PassaroModel({
        nome: 'chimango',
        nomeIngles: 'Chimango Caracara',
        nomeCientifico: 'Milvago chimango',
        tamanho: '37',
        genero: '',
        familia: 'Falconidae',
        habitat: 'campo seco baixo, campo seco alto, campo alagado',
        cor: 'Marrom'
    }),
    new PassaroModel({
        nome: 'falcão-de-coleira',
        nomeIngles: 'Aplomado Falcon',
        nomeCientifico: 'Falco femoralis',
        tamanho: '33-38',
        genero: '',
        familia: 'Falconidae',
        habitat: 'campo seco baixo, campo seco alto, campo com árvores, campo alagado',
        cor: 'Marrom'
    }),
    new PassaroModel({
        nome: 'carrapateiro',
        nomeIngles: 'Yellow-headed Caracara',
        nomeCientifico: 'Milvago chimachima',
        tamanho: '37',
        genero: '',
        familia: 'Falconidae',
        habitat: '',
        cor: 'Marrom'
    }),
    new PassaroModel({
        nome: 'mocho-dos-banhados',
        nomeIngles: 'Short-eared Owl',
        nomeCientifico: 'Asio flammeus',
        tamanho: '38',
        genero: '',
        familia: 'Strigidae',
        habitat: 'banhado com vegetação altas',
        cor: 'Marrom'
    }),
    new PassaroModel({
        nome: 'coruja-buraqueira',
        nomeIngles: 'Burrowing Owl',
        nomeCientifico: 'Athene cunicularia',
        tamanho: '25',
        genero: '',
        familia: 'Strigidae',
        habitat: 'campo seco baixo',
        cor: 'Marrom'
    }),
    new PassaroModel({
        nome: 'corucão',
        nomeIngles: 'Nacunda Nighthawk',
        nomeCientifico: 'Podager nacunda',
        tamanho: '28',
        genero: '',
        familia: 'Caprimulgidae',
        habitat: 'campo seco baixo',
        cor: 'Marrom'
    }),
    new PassaroModel({
        nome: 'bacurau-norte-americano',
        nomeIngles: 'Common Nighthawk',
        nomeCientifico: 'Chordeiles minor',
        tamanho: '23',
        genero: '',
        familia: 'Caprimulgidae',
        habitat: 'campo seco baixo, campo com árvores',
        cor: 'Marrom'
    }),
    new PassaroModel({
        nome: 'bacurau-da-telha',
        nomeIngles: 'Band-winged Nightjar',
        nomeCientifico: 'Hydropsalis longirostris',
        tamanho: '23',
        genero: '',
        familia: 'Caprimulgidae',
        habitat: 'campo seco baixo, campo com árvores',
        cor: 'Marrom'
    }),
    new PassaroModel({
        nome: 'curiango-do-banhado',
        nomeIngles: 'Sickle-winged Nightjar',
        nomeCientifico: 'Hydropsalis anomala',
        tamanho: '17',
        genero: '',
        familia: 'Caprimulgidae',
        habitat: 'campo seco alto',
        cor: 'Marrom'
    }),
    new PassaroModel({
        nome: 'perdigão',
        nomeIngles: 'Red-winged Tinamou',
        nomeCientifico: 'Rhynchotus rufescens',
        tamanho: '38',
        genero: '',
        familia: 'Tinamidae',
        habitat: 'campo seco alto',
        cor: 'Marrom'
    }),
    new PassaroModel({
        nome: 'perdiz',
        nomeIngles: 'Spotted Nothura',
        nomeCientifico: 'Nothura maculosa',
        tamanho: '25',
        genero: '',
        familia: 'Tinamidae',
        habitat: 'campo seco baixo, campo seco alto, campo com árvores',
        cor: 'Marrom'
    }),
    new PassaroModel({
        nome: 'batuiruçu',
        nomeIngles: 'American Golden-Plover',
        nomeCientifico: 'Pluvialis dominica',
        tamanho: '22',
        genero: '',
        familia: 'Charadriidae',
        habitat: 'campo seco baixo, campo alagado',
        cor: 'Marrom'
    }),
    new PassaroModel({
        nome: 'batuíra-de-peito-avermelhado',
        nomeIngles: 'Rufous-chested Dotterel',
        nomeCientifico: 'Charadrius modestus',
        tamanho: '18',
        genero: '',
        familia: 'Charadriidae',
        habitat: 'campo seco baixo, campo alagado',
        cor: 'Marrom'
    }),
    new PassaroModel({
        nome: 'maçarico-do-campo',
        nomeIngles: 'Upland Sandpiper',
        nomeCientifico: 'Bartramia longicauda',
        tamanho: '25',
        genero: '',
        familia: 'Scolopacidae',
        habitat: 'campo seco baixo',
        cor: 'Marrom'
    }),
    new PassaroModel({
        nome: 'narceja',
        nomeIngles: 'South American Snipe',
        nomeCientifico: 'Gallinago paraguaiae',
        tamanho: '23',
        genero: '',
        familia: 'Scolopacidae',
        habitat: 'campo alagado, banhado com vegetação alta',
        cor: 'Marrom'
    }),
    new PassaroModel({
        nome: 'maçarico-de-colete',
        nomeIngles: 'Pectoral Sandpiper',
        nomeCientifico: 'Calidris melanotos',
        tamanho: '18',
        genero: '',
        familia: 'Scolopacidae',
        habitat: 'campo alagado',
        cor: 'Marrom'
    }),
    new PassaroModel({
        nome: 'maçarico-acanelado',
        nomeIngles: 'Buff-breasted Sandpiper',
        nomeCientifico: 'Calidris subruficollis',
        tamanho: '17',
        genero: '',
        familia: 'Scolopacidae',
        habitat: 'campo seco baixo',
        cor: 'Marrom'
    }),
    new PassaroModel({
        nome: 'maçarico-de-sobre-branco',
        nomeIngles: 'White-rumped Sandpiper',
        nomeCientifico: 'Calidris fuscicollis',
        tamanho: '15',
        genero: '',
        familia: 'Scolopacidae',
        habitat: 'campo alagado',
        cor: 'Marrom'
    }),
    new PassaroModel({
        nome: 'sabiá-do-campo',
        nomeIngles: 'Chalk-browed Mockingbird',
        nomeCientifico: 'Mimus saturninus',
        tamanho: '25',
        genero: '',
        familia: 'Mimidae',
        habitat: 'campo com árvores',
        cor: 'Marrom'
    }),
    new PassaroModel({
        nome: 'calhandra-de-três-rabos',
        nomeIngles: 'White-banded Mockingbird',
        nomeCientifico: 'Mimus triurus',
        tamanho: '20',
        genero: '',
        familia: 'Mimidae',
        habitat: 'campo com árvores',
        cor: 'Marrom'
    }),
    new PassaroModel({
        nome: 'coperete',
        nomeIngles: 'Brown Cacholote',
        nomeCientifico: 'Pseudoseisura lophotes',
        tamanho: '25',
        genero: '',
        familia: 'Furnariidae',
        habitat: 'campo seco baixo, campo com árvores',
        cor: 'Marrom'
    }),
    new PassaroModel({
        nome: 'pedreiro',
        nomeIngles: 'Long-tailed Cinclodes',
        nomeCientifico: 'Cinclodes pabsti',
        tamanho: '22',
        genero: '',
        familia: 'Furnariidae',
        habitat: 'campo seco baixo',
        cor: 'Marrom'
    }),
    new PassaroModel({
        nome: 'pedreiro-dos-andes',
        nomeIngles: 'Buff-winged Cinclodes',
        nomeCientifico: 'Cinclodes fuscus',
        tamanho: '16',
        genero: '',
        familia: 'Furnariidae',
        habitat: 'campo alagado',
        cor: 'Marrom'
    }),
    new PassaroModel({
        nome: 'joão-de-barro',
        nomeIngles: 'Rufous Hornero',
        nomeCientifico: 'Furnarius rufus',
        tamanho: '18',
        genero: '',
        familia: 'Furnariidae',
        habitat: 'campo seco baixo, campo com árvores',
        cor: 'Marrom'
    }),
    new PassaroModel({
        nome: 'cochicho',
        nomeIngles: 'Firewood-gatherer',
        nomeCientifico: 'Anumbius annumbi',
        tamanho: '18',
        genero: '',
        familia: 'Furnariidae',
        habitat: 'campo com árvores',
        cor: 'Marrom'
    }),
    new PassaroModel({
        nome: 'lenheiro-platino',
        nomeIngles: 'Asthenes hudsoni',
        nomeCientifico: 'Hudson’s Canastero',
        tamanho: '17',
        genero: '',
        familia: 'Furnariidae',
        habitat: 'campo seco alto',
        cor: 'Marrom'
    }),
    new PassaroModel({
        nome: 'bichoita',
        nomeIngles: 'Chotoy Spinetail',
        nomeCientifico: 'Schoeniophylax phryganophila',
        tamanho: '18-22',
        genero: '',
        familia: 'Furnariidae',
        habitat: 'campo com árvores',
        cor: 'Marrom'
    }),
    new PassaroModel({
        nome: 'joão-da-palha',
        nomeIngles: 'Curve-billed Reedhaunter',
        nomeCientifico: 'Limnornis curvirostris',
        tamanho: '16',
        genero: '',
        familia: 'Furnariidae',
        habitat: 'banhado com vegetação alta',
        cor: 'Marrom'
    }),
    new PassaroModel({
        nome: 'arredio-do-gravatá',
        nomeIngles: 'Straight-billed Reedhaunter',
        nomeCientifico: 'Limnoctites rectirostris',
        tamanho: '15',
        genero: '',
        familia: 'Furnariidae',
        habitat: 'banhado com vegetação alta',
        cor: 'Marrom'
    }),
    new PassaroModel({
        nome: 'tio-tio',
        nomeIngles: 'Freckle-breasted Thornbird',
        nomeCientifico: 'Phacellodomus striaticollis',
        tamanho: '16',
        genero: '',
        familia: 'Furnariidae',
        habitat: '',
        cor: 'Marrom'
    }),
    new PassaroModel({
        nome: 'arredio-de-papo-manchado',
        nomeIngles: 'Sulphur-bearded Spinetail',
        nomeCientifico: 'Cranioleuca sulphurifera',
        tamanho: '15',
        genero: '',
        familia: 'Furnariidae',
        habitat: 'banhado com vegetação altas',
        cor: 'Marrom'
    }),
    new PassaroModel({
        nome: 'lenheiro',
        nomeIngles: 'Short-billed canastero',
        nomeCientifico: 'Asthenes baeri',
        tamanho: '15',
        genero: '',
        familia: 'Furnariidae',
        habitat: 'campo com árvores',
        cor: 'Marrom'
    }),
    new PassaroModel({
        nome: 'curutié',
        nomeIngles: 'Yellow-chinned Spinetail',
        nomeCientifico: 'Certhiaxis cinnamomeus',
        tamanho: '14',
        genero: '',
        familia: 'Furnariidae',
        habitat: 'banhado com vegetação alta, banhado',
        cor: 'Marrom'
    }),
    new PassaroModel({
        nome: 'curutié',
        nomeIngles: 'Bay-capped Wren-Spinetail',
        nomeCientifico: 'Spartonoica maluroides',
        tamanho: '13',
        genero: '',
        familia: 'Furnariidae',
        habitat: 'campo seco alto, banhado com vegetação alta',
        cor: 'Marrom'
    }),
    new PassaroModel({
        nome: 'polícia-inglesa',
        nomeIngles: 'White-browed Blackbird',
        nomeCientifico: 'Sturnella superciliaris',
        tamanho: '17',
        genero: 'Fêmea',
        familia: 'Icteridae',
        habitat: 'campo seco baixo, campo alagado',
        cor: 'Marrom'
    }),
    new PassaroModel({
        nome: 'arapuçu-platino',
        nomeIngles: 'Scimitar-billed Woodcreeper',
        nomeCientifico: 'Dryimornis bridgesii',
        tamanho: '33',
        genero: '',
        familia: 'Dendrocolaptidae',
        habitat: 'campo seco baixo, campo com árvores',
        cor: 'Marrom'
    }),
    new PassaroModel({
        nome: 'arapaçu-de-cerrado',
        nomeIngles: 'Narrowbilled Woodcreepe',
        nomeCientifico: 'Lepidocolaptes angustirostris',
        tamanho: '20',
        genero: '',
        familia: 'Dendrocolaptidae',
        habitat: 'campo com árvores',
        cor: 'Marrom'
    }),
    new PassaroModel({
        nome: 'curriqueiro',
        nomeIngles: 'Common Miner',
        nomeCientifico: 'Geositta cunicularia',
        tamanho: '14',
        genero: '',
        familia: 'Scleruridae',
        habitat: 'campo seco baixo',
        cor: 'Marrom'
    }),
    new PassaroModel({
        nome: 'caminheiro-de-unha-curta',
        nomeIngles: 'Short-billed Pipit',
        nomeCientifico: 'Anthus furcatus',
        tamanho: '14',
        genero: '',
        familia: 'Motacillidae',
        habitat: 'campo seco baixo',
        cor: 'Marrom'
    }),
    new PassaroModel({
        nome: 'caminheiro-grande',
        nomeIngles: 'Ochre-breasted Pipit',
        nomeCientifico: 'Anthus natterer',
        tamanho: '14',
        genero: '',
        familia: 'Motacillidae',
        habitat: 'campo seco alto, campo seco baixo',
        cor: 'Marrom'
    }),
    new PassaroModel({
        nome: 'caminheiro-de-espora',
        nomeIngles: 'Correndera Pipit',
        nomeCientifico: 'Anthus correndera',
        tamanho: '14',
        genero: '',
        familia: 'Motacillidae',
        habitat: 'campo seco baixo',
        cor: 'Marrom'
    }),
    new PassaroModel({
        nome: 'caminheiro-de-barriga-acanelada',
        nomeIngles: 'Hellmayr’s Pipit',
        nomeCientifico: 'Anthus hellmayr',
        tamanho: '14',
        genero: '',
        familia: 'Motacillidae',
        habitat: 'campo seco baixo, campo seco alto, campo com árvores',
        cor: 'Marrom'
    }),
    new PassaroModel({
        nome: 'caminheiro-zumbidor',
        nomeIngles: 'Yellowish Pipit',
        nomeCientifico: 'Anthus lutescens',
        tamanho: '12',
        genero: '',
        familia: 'Motacillidae',
        habitat: 'campo seco baixo, campo alagado',
        cor: 'Marrom'
    }),
    new PassaroModel({
        nome: 'birro',
        nomeIngles: 'Cliff Flycatcher',
        nomeCientifico: 'Hirundinea ferruginea',
        tamanho: '16',
        genero: '',
        familia: 'Tyrannidae',
        habitat: 'campo seco baixo, campo com árvores',
        cor: 'Marrom'
    }),
    new PassaroModel({
        nome: 'papa-moscas-do-campo',
        nomeIngles: 'Sharp-tailed Grass-Tyrant',
        nomeCientifico: 'Culicivora caudacuta',
        tamanho: '10',
        genero: '',
        familia: 'Tyrannidae',
        habitat: 'campo seco alto, banhado com vegetação alta',
        cor: 'Marrom'
    }),
    new PassaroModel({
        nome: 'papa-moscas-canela',
        nomeIngles: 'Bearded Tachur',
        nomeCientifico: 'Polystictus pectoralis',
        tamanho: '9',
        genero: 'Macho',
        familia: 'Tyrannidae',
        habitat: 'campo seco alto, banhado com vegetação alta',
        cor: 'Marrom'
    }),
    new PassaroModel({
        nome: 'andorinha-de-bando',
        nomeIngles: 'Barn Swallow',
        nomeCientifico: 'Hirundo rustica',
        tamanho: '15',
        genero: '',
        familia: 'Hirundinidae',
        habitat: '',
        cor: 'Marrom'
    }),
    new PassaroModel({
        nome: 'andorinha-serradora',
        nomeIngles: 'Southern Rough-winged Swallow',
        nomeCientifico: 'Stelgidopteryx ruficollis',
        tamanho: '14',
        genero: '',
        familia: 'Hirundinidae',
        habitat: '',
        cor: 'Marrom'
    }),
    new PassaroModel({
        nome: 'andorinha-morena',
        nomeIngles: 'Tawny-headed Swallow',
        nomeCientifico: 'Alopochelidon fucata',
        tamanho: '11',
        genero: 'Jovem',
        familia: 'Hirundinidae',
        habitat: '',
        cor: 'Marrom'
    }),
    new PassaroModel({
        nome: 'quem-te-vestiu',
        nomeIngles: 'Black-and-rufous Warbling-Finch',
        nomeCientifico: 'Poospiza nigrorufa',
        tamanho: '13',
        genero: '',
        familia: 'Thraupidae',
        habitat: 'campo com árvores, banhado com vegetação grande',
        cor: 'Marrom'
    }),
    new PassaroModel({
        nome: 'coleiro-do-brejo',
        nomeIngles: 'Rusty-collared Seedeater',
        nomeCientifico: 'Sporophila collaris',
        tamanho: '11',
        genero: '',
        familia: 'Thraupidae',
        habitat: 'campo seco baixo, banhado com vegetação grande',
        cor: 'Marrom'
    }),
    new PassaroModel({
        nome: 'caboclinho-de-papo-branco',
        nomeIngles: 'Marsh Seedeater',
        nomeCientifico: 'Sporophila palustris',
        tamanho: '9',
        genero: 'Macho',
        familia: 'Thraupidae',
        habitat: 'campo alagado, banhado com vegetação grande',
        cor: 'Marrom'
    }),
    new PassaroModel({
        nome: 'caboclinho-de-papo-escuro',
        nomeIngles: 'Dark-throated Seedeater',
        nomeCientifico: 'Sporophila ruficollis',
        tamanho: '9',
        genero: 'Macho',
        familia: 'Thraupidae',
        habitat: 'campo seco alto, banhado com vegetação grande',
        cor: 'Marrom'
    }),
    new PassaroModel({
        nome: 'Chestnut Seedeater',
        nomeIngles: 'Dark-throated Seedeater',
        nomeCientifico: 'Sporophila cinnamomea',
        tamanho: '9',
        genero: 'Macho',
        familia: 'Thraupidae',
        habitat: 'campo seco alto, banhado com vegetação grande',
        cor: 'Marrom'
    }),
    new PassaroModel({
        nome: 'caboclinho-de-barriga-vermelha',
        nomeIngles: 'Tawny-bellied Seedeater',
        nomeCientifico: 'Sporophila hypoxantha',
        tamanho: '9',
        genero: 'Macho',
        familia: 'Thraupidae',
        habitat: 'campo seco alto, banhado com vegetação grande',
        cor: 'Marrom'
    }),
    new PassaroModel({
        nome: 'saracuruçu',
        nomeIngles: 'Giant Wood-Rail',
        nomeCientifico: 'Aramides ypecaha',
        tamanho: '42',
        genero: '',
        familia: 'Ralidae',
        habitat: 'campo alagado, banhado com vegetação alta',
        cor: 'Verde'
    }),
    new PassaroModel({
        nome: 'três-potes',
        nomeIngles: 'Grey-necked Wood-Rail',
        nomeCientifico: 'Aramides cajaneus',
        tamanho: '36',
        genero: '',
        familia: 'Ralidae',
        habitat: 'banhado com vegetação alta',
        cor: 'Verde'
    }),
    new PassaroModel({
        nome: 'caturrita',
        nomeIngles: 'Monk Parakeet',
        nomeCientifico: 'Myiopsitta monachus',
        tamanho: '27',
        genero: '',
        familia: 'Psittacidae',
        habitat: 'campo seco baixo, campo com árores',
        cor: 'Verde'
    }),
    new PassaroModel({
        nome: 'sabiá-do-banhado',
        nomeIngles: 'Great Pampa-Finch',
        nomeCientifico: 'Embernagra platensis',
        tamanho: '20',
        genero: '',
        familia: 'Thraupidae',
        habitat: 'campo seco alto, campo com árores, campo alagado, banhado com vegetação alta',
        cor: 'Verde'
    }),
    new PassaroModel({
        nome: 'canário-do-campo',
        nomeIngles: 'Wedge-tailed Grass-Finch',
        nomeCientifico: 'Emberizoides herbicola',
        tamanho: '18',
        genero: '',
        familia: 'Thraupidae',
        habitat: 'campo seco alto, campo alagado',
        cor: 'Verde'
    }),
    new PassaroModel({
        nome: 'canário-do-brejo',
        nomeIngles: 'Lesser Grass-Finch',
        nomeCientifico: 'Emberizoides ypiranganus',
        tamanho: '16',
        genero: '',
        familia: 'Thraupidae',
        habitat: 'campo seco alto, campo com árores, campo alagado',
        cor: 'Verde'
    }),
    new PassaroModel({
        nome: 'guaracava-grande',
        nomeIngles: 'Large Elaenia',
        nomeCientifico: 'Elaenia spectabilis',
        tamanho: '18',
        genero: '',
        familia: 'Tyrannidae',
        habitat: 'campo com árores',
        cor: 'Verde'
    }),
    new PassaroModel({
        nome: 'tucão',
        nomeIngles: 'Highland Elaenia',
        nomeCientifico: 'Elaenia obscura',
        tamanho: '16',
        genero: '',
        familia: 'Tyrannidae',
        habitat: 'campo com árores',
        cor: 'Verde'
    }),
    new PassaroModel({
        nome: 'bico-reto-azul',
        nomeIngles: 'Blue-tufted Starthroat',
        nomeCientifico: 'Heliomaster furcifer',
        tamanho: '13',
        genero: 'Fêmea',
        familia: 'Trochilidae',
        habitat: 'campo com árores',
        cor: 'Verde'
    }),
    new PassaroModel({
        nome: 'beija-flor-de-bico-curvo',
        nomeIngles: 'White-tailed Goldenthroat',
        nomeCientifico: 'Polytmus guaynumbi',
        tamanho: '10',
        genero: 'Fêmea',
        familia: 'Trochilidae',
        habitat: 'campo com árores',
        cor: 'Verde'
    }),
    new PassaroModel({
        nome: 'besourinho-de-bico-vermelho',
        nomeIngles: 'Glittering-bellied Emerald',
        nomeCientifico: 'Chlorostilbon lucidus',
        tamanho: '7',
        genero: 'Macho',
        familia: 'Trochilidae',
        habitat: 'campo com árores, banhado com vegetação alta',
        cor: 'Verde'
    }),
    new PassaroModel({
        nome: 'beija-flor-dourado',
        nomeIngles: 'Gilded Hummingbird',
        nomeCientifico: 'Hylocharis chrysura',
        tamanho: '8',
        genero: '',
        familia: 'Trochilidae',
        habitat: 'campo com árores',
        cor: 'Verde'
    }),
    new PassaroModel({
        nome: 'carrapateiro',
        nomeIngles: 'Yellow-headed Caracara',
        nomeCientifico: 'Milvago chimachima',
        tamanho: '37',
        genero: '',
        familia: 'Falconidae',
        habitat: '',
        cor: 'Bege'
    }),
    new PassaroModel({
        nome: 'anu-branco',
        nomeIngles: 'Guira Cuckoo',
        nomeCientifico: 'Guira guira',
        tamanho: '36',
        genero: '',
        familia: 'Cuculidae',
        habitat: '',
        cor: 'Bege'
    }),
    new PassaroModel({
        nome: 'papa-lagarta-acanelado',
        nomeIngles: 'Dark-billed Cuckoo',
        nomeCientifico: 'Coccyzus melacoryphus',
        tamanho: '28',
        genero: '',
        familia: 'Cuculidae',
        habitat: 'campo com árores',
        cor: 'Bege'
    }),
    new PassaroModel({
        nome: 'asa-de-telha',
        nomeIngles: 'Bay-winged Cowbird',
        nomeCientifico: 'Agelaioides badius',
        tamanho: '18',
        genero: '',
        familia: 'Icteridae',
        habitat: 'campo com árores',
        cor: 'Bege'
    }),
    new PassaroModel({
        nome: 'tico-tico-do-campo',
        nomeIngles: 'Grassland Sparrow',
        nomeCientifico: 'Ammodramus humeralis',
        tamanho: '11',
        genero: '',
        familia: 'Passerellidae',
        habitat: 'campo seco alto',
        cor: 'Bege'
    }),
    new PassaroModel({
        nome: 'tico-tico',
        nomeIngles: 'Rufous-collared Sparrow',
        nomeCientifico: 'Zonotrichia capensis',
        tamanho: '12',
        genero: '',
        familia: 'Passerellidae',
        habitat: '',
        cor: 'Bege'
    }),
    new PassaroModel({
        nome: 'corruíra-do-campo',
        nomeIngles: 'Marsh Wren',
        nomeCientifico: 'Cistothorus platensis',
        tamanho: '10',
        genero: '',
        familia: 'Troglodytidae',
        habitat: 'campo seco alto',
        cor: 'Bege'
    }),
    new PassaroModel({
        nome: 'corruíra',
        nomeIngles: 'Southern House Wren',
        nomeCientifico: 'Troglodytes musculus',
        tamanho: '12',
        genero: '',
        familia: 'Troglodytidae',
        habitat: '',
        cor: 'Bege'
    }),
    new PassaroModel({
        nome: 'viuvinha-de-óculos',
        nomeIngles: 'Spectacled Tyrant',
        nomeCientifico: 'Hymenops perspicillatus',
        tamanho: '13',
        genero: 'Fêmea',
        familia: 'Tyrannidae',
        habitat: 'campo seco alto, campo alagado, banhado',
        cor: 'Bege'
    }),
    new PassaroModel({
        nome: 'tico-tico-do-banhado',
        nomeIngles: 'Long-tailed Reed-Finch',
        nomeCientifico: 'Donacospiza albifrons',
        tamanho: '14',
        genero: '',
        familia: 'Thraupidae',
        habitat: 'campo seco alto, banhado com vegetação alta',
        cor: 'Bege'
    }),
    new PassaroModel({
        nome: 'canário-da-terra-verdadeiro',
        nomeIngles: 'Saffron Finch',
        nomeCientifico: 'Sicalis flaveola',
        tamanho: '12',
        genero: 'Fêmea',
        familia: 'Thraupidae',
        habitat: 'campo seco baixo, campo seco alto, campo com árores',
        cor: 'Bege'
    }),
    new PassaroModel({
        nome: 'patativa-tropeira',
        nomeIngles: 'Tropeiro Seedeater',
        nomeCientifico: 'Sporophila beltoni',
        tamanho: '12',
        genero: 'Fêmea',
        familia: 'Thraupidae',
        habitat: 'campo seco alto, campo com árores',
        cor: 'Bege'
    }),
    new PassaroModel({
        nome: 'coleiro-do-brejo',
        nomeIngles: 'Rusty-collared Seedeater',
        nomeCientifico: 'Sporophila collaris',
        tamanho: '11',
        genero: 'Fêmea',
        familia: 'Thraupidae',
        habitat: 'campo seco baixo, banhado com vegetação alta',
        cor: 'Bege'
    }),
    new PassaroModel({
        nome: 'caboclinho-de-barriga-vermelha',
        nomeIngles: 'Tawny-bellied Seedeater',
        nomeCientifico: 'Sporophila hypoxantha',
        tamanho: '9',
        genero: 'Fêmea',
        familia: 'Thraupidae',
        habitat: 'campo seco alto, banhado com vegetação alta',
        cor: 'Bege'
    }),
    new PassaroModel({
        nome: 'caboclinho',
        nomeIngles: 'Pearly-bellied Seedeater',
        nomeCientifico: 'Sporophila pileata',
        tamanho: '9',
        genero: 'Macho',
        familia: 'Thraupidae',
        habitat: 'campo seco alto, banhado com vegetação alta',
        cor: 'Bege'
    }),
    new PassaroModel({
        nome: 'caboclinho',
        nomeIngles: 'Pearly-bellied Seedeater',
        nomeCientifico: 'Sporophila pileata',
        tamanho: '9',
        genero: 'Fêmea',
        familia: 'Thraupidae',
        habitat: 'campo seco alto, banhado com vegetação alta',
        cor: 'Bege'
    }),
    new PassaroModel({
        nome: 'caboclinho-de-barriga-preta',
        nomeIngles: 'Black-bellied Seedeater',
        nomeCientifico: 'Sporophila melanogaster',
        tamanho: '9',
        genero: 'Fêmea',
        familia: 'Thraupidae',
        habitat: 'campo seco alto, banhado com vegetação alta',
        cor: 'Bege'
    }),
    new PassaroModel({
        nome: 'caboclinho-de-chapéu-cinzento',
        nomeIngles: 'Chestnut Seedeater',
        nomeCientifico: 'Sporophila cinnamomea',
        tamanho: '9',
        genero: 'Fêmea',
        familia: 'Thraupidae',
        habitat: 'campo seco alto, banhado com vegetação alta',
        cor: 'Bege'
    }),
    new PassaroModel({
        nome: 'pica-pau-do-campo',
        nomeIngles: 'Campo Flicker',
        nomeCientifico: 'Colaptes campestris',
        tamanho: '28',
        genero: 'Fêmea',
        familia: 'Picidae',
        habitat: 'campo seco alto, campo com árores',
        cor: 'Amarelo'
    }),
    new PassaroModel({
        nome: 'bem-te-vi',
        nomeIngles: 'Great Kiskadee',
        nomeCientifico: 'Pitangus sulphuratus',
        tamanho: '23',
        genero: '',
        familia: 'Tyrannidae',
        habitat: '',
        cor: 'Amarelo'
    }),
    new PassaroModel({
        nome: 'suiriri-cavaleiro',
        nomeIngles: 'Cattle Tyrant',
        nomeCientifico: 'Machetornis rixosa',
        tamanho: '17',
        genero: '',
        familia: 'Tyrannidae',
        habitat: 'campo seco alto, campo com árores',
        cor: 'Amarelo'
    }),
    new PassaroModel({
        nome: 'suiriri',
        nomeIngles: 'Tropical Kingbird',
        nomeCientifico: 'Tyrannus melancholicus',
        tamanho: '32-42',
        genero: '',
        familia: 'Tyrannidae',
        habitat: 'campo seco baixo, campo com árores',
        cor: 'Amarelo'
    }),
    new PassaroModel({
        nome: 'suiriri-pequeno',
        nomeIngles: 'Yellow-browed Tyrant',
        nomeCientifico: 'Satrapa icterophrys',
        tamanho: '15',
        genero: '',
        familia: 'Tyrannidae',
        habitat: 'campo seco alto, campo com árores',
        cor: 'Amarelo'
    }),
    new PassaroModel({
        nome: 'pia-cobra',
        nomeIngles: 'Yellowthroat',
        nomeCientifico: 'Geothlypis aequinoctialis',
        tamanho: '13',
        genero: 'Macho',
        familia: 'Parulidae',
        habitat: 'campo seco alto, campo com árores, banhado com vegetação alta',
        cor: 'Amarelo'
    }),
    new PassaroModel({
        nome: 'canário-da-terra-verdadeiro',
        nomeIngles: 'Saffron Finch',
        nomeCientifico: 'Sicalis flaveola',
        tamanho: '12',
        genero: 'Macho',
        familia: 'Thraupidae',
        habitat: 'campo seco baixo, campo seco alto, campo com árores',
        cor: 'Amarelo'
    }),
    new PassaroModel({
        nome: 'canário-rasteiro',
        nomeIngles: 'Stripe-tailed Yellow-Finch',
        nomeCientifico: 'Sicalis citrina',
        tamanho: '12',
        genero: 'Macho',
        familia: 'Thraupidae',
        habitat: 'campo seco alto',
        cor: 'Amarelo'
    }),
    new PassaroModel({
        nome: 'tipio',
        nomeIngles: 'Grassland Yellow-Finch',
        nomeCientifico: 'Sicalis luteola',
        tamanho: '12',
        genero: 'Macho',
        familia: 'Thraupidae',
        habitat: 'campo seco alto, campo com árores',
        cor: 'Amarelo'
    }),
    new PassaroModel({
        nome: 'tipio',
        nomeIngles: 'Grassland Yellow-Finch',
        nomeCientifico: 'Sicalis luteola',
        tamanho: '12',
        genero: 'Fêmea',
        familia: 'Thraupidae',
        habitat: 'campo seco alto, campo com árores',
        cor: 'Amarelo'
    }),
    new PassaroModel({
        nome: 'amarelinho-do-junco',
        nomeIngles: 'Warbling Doradito',
        nomeCientifico: 'Pseudocolopteryx flaviventris',
        tamanho: '10',
        genero: '',
        familia: 'Tyrannidae',
        habitat: 'banhado com vegetação alta',
        cor: 'Amarelo'
    }),
    new PassaroModel({
        nome: 'papa-piri',
        nomeIngles: 'Many-colored Rush Tyrant',
        nomeCientifico: 'Tachuris rubrigastra',
        tamanho: '11',
        genero: '',
        familia: 'Tachurisidae',
        habitat: 'banhado com vegetação alta',
        cor: 'Amarelo'
    }),
    new PassaroModel({
        nome: 'cardeal-amarelo',
        nomeIngles: 'Yellow Cardinal',
        nomeCientifico: 'Gubernatrix cristata',
        tamanho: '19',
        genero: 'Macho',
        familia: 'Thraupidae',
        habitat: '',
        cor: 'Amarelo'
    }),
    new PassaroModel({
        nome: 'cardeal-amarelo',
        nomeIngles: 'Yellow Cardinal',
        nomeCientifico: 'Gubernatrix cristata',
        tamanho: '19',
        genero: 'Fêmea',
        familia: 'Thraupidae',
        habitat: '',
        cor: 'Amarelo'
    }),
];


export default async () => {
    let numeroPassaros = await PassaroModel.countDocuments({}).exec();

    if (numeroPassaros == 0) {
        for (let passaro of passaros) {
            await passaro.save()
        }
    }
}
