import { Passaro } from "./passaro_Interface";

export interface Avistamento {
    local: String;
    data: Date;
    hora: Date;
    pagina: Number;
    linha: Number;
    coluna: Number;
    passaro: Passaro;
}