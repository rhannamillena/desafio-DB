export interface Passaro {
    nome: String,
    nomeIngles: String,
    nomeCientifico: String,
    genero: String,
    familia: String,
    tamanho: String,
    habitat: String,
    cor: String,
}
