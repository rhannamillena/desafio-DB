import { MongoMemoryServer } from 'mongodb-memory-server';
import mongoose from 'mongoose';

class BdEmMemoria {
    private mongoServer?: MongoMemoryServer;
    private static instancia: BdEmMemoria;

    public static getInstancia() {
        if (!BdEmMemoria.instancia) {
            BdEmMemoria.instancia = new BdEmMemoria();
        }
        return BdEmMemoria.instancia
    }

    public async abrir() {
        try {
            this.mongoServer = await MongoMemoryServer.create();
            const uri = this.mongoServer.getUri();
            await mongoose.connect(uri);
        } catch (error) {
            console.log("Falha de criação do BD em memória.");
            console.log(error);
            throw error;
        }
    }

    public async fechar() {
        try {
            await mongoose.connection.dropDatabase();
            await mongoose.disconnect();

            if (this.mongoServer) {
                await this.mongoServer.stop();
            }
        } catch (error) {
            console.log("Falha de encerramento do BD em memória.");
            console.log(error);
            throw error;
        }
    }

    public async limpar() {
        try {
            const colecoes = mongoose.connection.collections;
            for (const nomeColecao in colecoes) {
                if (nomeColecao !== "passaros") {
                    const colecao = colecoes[nomeColecao];
                    await colecao.deleteMany({});
                }
            }
        } catch (error) {
            console.log("Falha de limpeza das coleções do BD em memória.");
            console.log(error);
            throw error;
        }
    }
}

export default BdEmMemoria.getInstancia();