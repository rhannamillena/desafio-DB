# Catálogo de Aves

O projeto foi desenvolvido por Micheli Ferreira e Rhanna Millena para um desafio da DB Server.



## 🚀 Começando

Execute o seguinte comando para baixar o projeto:
```
git clone git@gitlab.com:rhannamillena/desafio-DB.git
```

### 📋 Pré-requisitos

Para executar o projeto é necessário possuir o Node JS e NPM instalados na sua máquina.

### 🔧 Instalação Backend

Primeiro entre na pasta do Backend com o comando
```
cd Backend
```

E em seguida baixe todas as dependencias dele:
```
npm install
```

Agora para executar o Backend da aplicação basta executar
```
npm run start
```

Agora o backend da aplicação já vai estar disponível na porta 8081, porém essa configuração pode ser alterada no .env

## ⚙️ Executando os testes

Para executar os testes basta acessar a pasta "Backend" e executar o seguinte comando

```
npm run test
```

### 🔧 Instalação Frontend

Primeiro entre na pasta do Frontend com o comando
```
cd Frontend
```

E em seguida baixe todas as dependencias dele:
```
npm install
```

Agora para executar o Frontend da aplicação basta executar
```
npm run start
```

Agora o backend da aplicação já vai estar disponível na porta 8080, porém essa configuração pode ser alterada no .env

## 🛠️ Construído com

Mencione as ferramentas que você usou para criar seu projeto

* [Node.js](https://nodejs.org/en/) - Back-end
* [Jest](https://jestjs.io/pt-BR/) - Testes
* [MongoDB](https://www.mongodb.com) - Banco de Dados
* [React](https://pt-br.reactjs.org) - Front-end

## ✒️ Autores

Mencione todos aqueles que ajudaram a levantar o projeto desde o seu início

* **Desenvolvedora** - *Micheli Ferreira* - [micheliferreira846](https://gitlab.com/micheliferreira846)
* **Desenvolvedora** - *Rhanna Millena* - [rhannamillena](https://gitlab.com/rhannamillena)